﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TechnicalTest.Models
{
    public class ProductForm
    {
        public string productName { get; set; }
        public string productNumber { get; set; }
        public string productColor { get; set; }
        public decimal productCost { get; set; }
        public decimal listPrice { get; set; }
        public string size { get; set; }
        public int weight { get; set; }
        public int productCategory { get; set; }
        public int productModel { get; set; }
        public DateTime sellStartDate { get; set; }
        public DateTime sellEndDate { get; set; }
        public DateTime discontinuedDate { get; set; }
        public byte[] thumbNailPhoto { get; set; }
        public string thumbNailPhotoFileName { get; set; }

    }
}