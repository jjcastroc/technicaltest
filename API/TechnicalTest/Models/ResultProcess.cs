﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TechnicalTest.Models
{
    public class ResultProcess
    {
        public string Code { get; set;}

        public string Type { get; set;}

        public string Message { get; set; }

    }
}