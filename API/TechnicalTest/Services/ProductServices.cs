﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TechnicalTest.Models;

namespace TechnicalTest.Services
{
    public class ProductServices : IProductServices<Product, int>
    {
        [Dependency]
        public prodigiousEntities context { get; set; }

        public bool Delete(int id)
        {
            var result = false;
            Product product = context.Products.Find(id);
            if (product != null)
            {
                context.Products.Remove(product);
                context.SaveChanges();
                result = true;
            }
            return result;
        }

        public IEnumerable<dynamic> Get()
        {
            return context.Products.Select(o => new {
                ProductID = o.ProductID,
                Name = o.Name,
                ProductCategoryID = o.ProductCategoryID,
                ThumbNailPhoto = o.ThumbNailPhoto,
                ThumbNailPhotoFileName = o.ThumbnailPhotoFileName,
                SellStartDate = o.SellStartDate
            }).OrderByDescending(o=> o.SellStartDate).ToList();
        }


        public IEnumerable<dynamic> GetByCategoryId(int idCategory)
        {
            return context.Products.Where(o=> o.ProductCategoryID == idCategory).Select(o => new {
                ProductID = o.ProductID,
                Name = o.Name,
                ProductCategoryID = o.ProductCategoryID,
                ThumbNailPhoto = o.ThumbNailPhoto,
                ThumbNailPhotoFileName = o.ThumbnailPhotoFileName,
                SellStartDate = o.SellStartDate
            }).OrderByDescending(o => o.SellStartDate).ToList();
        }

        public Product Get(int id)
        {
            return context.Products.Find(id);
        }

        public Product Post(Product entity)
        {
            entity.ModifiedDate = DateTime.Now;
            var product = context.Products.Add(entity);
            context.SaveChanges();
            return product;
        }

        public Product Put(int id, Product entity)
        {
            Product product = context.Products.Find(id);
            if (product != null && entity != null)
            {
                product.Color = entity.Color;
                product.DiscontinuedDate = entity.DiscontinuedDate;
                product.ListPrice = entity.ListPrice;
                product.ModifiedDate = DateTime.Now;
                product.Name = entity.Name;
                product.ProductCategoryID = entity.ProductCategoryID;
                product.ProductModelID = entity.ProductModelID;
                product.ProductNumber = entity.ProductNumber;
                product.rowguid = entity.rowguid;
                product.SellEndDate = entity.SellEndDate;
                product.SellStartDate = entity.SellStartDate;
                product.Size = entity.Size;
                product.StandardCost = entity.StandardCost;
                product.ThumbNailPhoto = entity.ThumbNailPhoto;
                product.ThumbnailPhotoFileName = entity.ThumbnailPhotoFileName;
                product.Weight = entity.Weight;
                context.SaveChanges();
            }
            return product;
        }
    }
}