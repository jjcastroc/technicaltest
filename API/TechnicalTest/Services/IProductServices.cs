﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnicalTest.Services
{
    public interface IProductServices<TEntity,in TPrimaryKey> where TEntity : class
    {

        IEnumerable<dynamic> Get();

        IEnumerable<dynamic> GetByCategoryId(int idCategory);

        TEntity Get(TPrimaryKey id);

        TEntity Post(TEntity entity);
        TEntity Put(TPrimaryKey id, TEntity entity);
        bool Delete(TPrimaryKey id);

    }
}
