﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TechnicalTest.Models;

namespace TechnicalTest.Services
{
    public class ProductModelServices : IProductModelServices<ProductModel, int>
    {
        [Dependency]
        public prodigiousEntities context { get; set; }

        public IEnumerable<ProductModel> Get()
        {
            return context.ProductModels.ToList();
        }

    }
}