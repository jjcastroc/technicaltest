﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TechnicalTest.Models;

namespace TechnicalTest.Services
{
    public class ProductCategoryServices : IProductCategoryServices<ProductCategory, int>
    {
        [Dependency]
        public prodigiousEntities context { get; set; }

        public IEnumerable<ProductCategory> Get()
        {
            return context.ProductCategories.ToList();
        }

        public ProductCategory Get(int id)
        {
            return context.ProductCategories.Find(id);
        }

    }
}