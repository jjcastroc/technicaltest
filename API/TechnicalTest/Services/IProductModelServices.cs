﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnicalTest.Services
{
    public interface IProductModelServices<TEntity,in TPrimaryKey> where TEntity : class
    {

        IEnumerable<TEntity> Get();

    }
}
