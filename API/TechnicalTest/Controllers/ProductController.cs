﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using TechnicalTest.Models;
using TechnicalTest.Services;

namespace TechnicalTest.Controllers
{
    public class ProductController : ApiController
    {
        private IProductServices<Product, int> productServices;

        public ProductController(IProductServices<Product, int> productServices)
        {
            this.productServices = productServices;
        }

        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        /// <summary>
        /// Get all products
        /// </summary>
        /// <returns></returns>
        public IEnumerable<dynamic> Get()
        {
            return productServices.Get();
        }

        /// <summary>
        /// Get all product by category id
        /// </summary>
        /// <returns></returns>
        public IEnumerable<dynamic> GetByCategoryId(int idCategory)
        {
            return productServices.GetByCategoryId(idCategory);
        }

        /// <summary>
        /// Get product by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ResponseType(typeof(Product))]
        public IHttpActionResult Get(int id)
        {
            return Ok(productServices.Get(id));
        }


        /// <summary>
        /// Create a product
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        [ResponseType(typeof(Product))]
        public IHttpActionResult Post([FromBody]ProductForm productForm)
        {
            Product product = translateFormToProduct(productForm);

            return Ok(productServices.Post(product));
        }


        /// <summary>
        /// Update a product
        /// </summary>
        /// <param name="id"></param>
        /// <param name="product"></param>
        /// <returns></returns>
        [ResponseType(typeof(Product))]
        public IHttpActionResult Put(int id, Product product)
        {
            return Ok(productServices.Put(id, product));
        }

        /// <summary>
        /// Delete a product
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ResponseType(typeof(Product))]
        public IHttpActionResult Delete(int id)
        {
            return Ok(productServices.Delete(id));
        }

        private Product translateFormToProduct(ProductForm productForm)
        {
            Product product = new Product();
            product.Color = productForm.productColor ;
            product.DiscontinuedDate = productForm.discontinuedDate==DateTime.MinValue? DateTime.Now : productForm.discontinuedDate;
            product.ListPrice = productForm.listPrice ;
            product.Name = productForm.productName ;
            product.ProductCategoryID = productForm.productCategory ;
            product.ProductModelID = productForm.productModel ;
            product.ProductNumber = productForm.productNumber ;
            product.SellEndDate = productForm.sellEndDate == DateTime.MinValue ? DateTime.Now : productForm.sellEndDate ;
            product.SellStartDate = productForm.sellStartDate ;
            product.Size = productForm.size ;
            product.StandardCost = productForm.productCost ;
            product.ThumbNailPhoto = productForm.thumbNailPhoto;
            product.ThumbnailPhotoFileName = productForm.thumbNailPhotoFileName ;
            product.Weight = productForm.weight;
            product.rowguid = Guid.NewGuid();

            return product;
        }

    }
}
