﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using TechnicalTest.Models;
using TechnicalTest.Services;

namespace TechnicalTest.Controllers
{
    public class ProductModelController : ApiController
    {
        private IProductModelServices<ProductModel, int> productModelServices;

        public ProductModelController(IProductModelServices<ProductModel, int> productModelServices)
        {
            this.productModelServices = productModelServices;
        }

        /// <summary>
        /// Get all product Model
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProductModel> Get()
        {
            var result = productModelServices.Get();
            return result;
        }

    }
}
