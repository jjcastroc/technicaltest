﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using TechnicalTest.Models;
using TechnicalTest.Services;

namespace TechnicalTest.Controllers
{
    public class ProductCategoryController : ApiController
    {
        private IProductCategoryServices<ProductCategory, int> productCategoryServices;

        public ProductCategoryController(IProductCategoryServices<ProductCategory, int> productCategoryServices)
        {
            this.productCategoryServices = productCategoryServices;
        }

        /// <summary>
        /// Get all product Categories
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProductCategory> Get()
        {
            var result = productCategoryServices.Get();
            return result;
        }


        /// <summary>
        /// Get product Category by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ResponseType(typeof(ProductCategory))]
        public IHttpActionResult Get(int id)
        {
            return Ok(productCategoryServices.Get(id));
        }

    }
}
