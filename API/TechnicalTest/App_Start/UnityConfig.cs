using Microsoft.Practices.Unity;
using System.Web.Http;
using TechnicalTest.Models;
using TechnicalTest.Services;
using Unity.WebApi;

namespace TechnicalTest
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            
            container.RegisterType<IProductServices<Product,int>,ProductServices>();
            container.RegisterType<IProductCategoryServices<ProductCategory, int>, ProductCategoryServices>();
            container.RegisterType<IProductModelServices<ProductModel, int>, ProductModelServices>();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}