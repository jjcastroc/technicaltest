      /*
       * Get string base64 from file
       * 
       */
      function uploadFile(){
            
            var image = $('input[type="file"]').get(0).files[0];
            $("#thumbnailPhotoFileName").val(image.name);
            
            var reader  = new FileReader();

            reader.onloadend = function () {
                var base =  reader.result.split(",");
                dataArray = base[1];
            }

            reader.readAsDataURL(image);
        }
        
        /*
         * Update the menu left sidebar
         * @param {type} categories
         * @returns {undefined}
         */
        function updatedMenuCategories(categories){
            $("#productCategory").html("");
            $("#productCategory").html('<option value=""> --Select Category-- </option>');
            for(var i=0;i<categories.length;i++)
            {
                $("#left-menu-sidebar").find(".nav-sidebar").append('<li><a onClick="getProductsByCategory('+categories[i].ProductCategoryID+')">'+categories[i].Name+'</a></li>');
                $("#productCategory").append('<option value="'+categories[i].ProductCategoryID+'">'+categories[i].Name+'</option>');
            }
            $('#productCategory').select2();
        }
        
        /**
         * Get all categories
         * @returns {undefined}
         */
        function getAllProductCategories(){
            $.ajax({
                method: "GET",
                url:'http://technicaltestj.azurewebsites.net/api/ProductCategory',
                datatype: 'json'
            }).done(function(response){
                updatedMenuCategories(response);
            }).fail(function(response) {
                $(".content-loader").hide();
                alert("An Error Occurred, please check the console for more information");
                console.log(response);
            });
        }
        
        
        /**
         * Get all product model
         * @returns {undefined}
         */
        function getAllProductModels(){
            $.ajax({
                method: "GET",
                url:'http://technicaltestj.azurewebsites.net/api/ProductModel',
                datatype: 'json'
            }).done(function(response){
                updatedSelectModel(response);
            }).fail(function(response) {
                $(".content-loader").hide();
                alert("An Error Occurred, please check the console for more information");
                console.log(response);
            });
        }
        
        /**
         * Update select model from form add product
         * @param {type} dataModel
         * @returns {undefined}
         */
        function updatedSelectModel(dataModel){
            $("#productModel").html("");
            $("#productModel").html('<option value=""> --Select Model-- </option>');
            for(var i=0;i<dataModel.length;i++)
            {
                $("#productModel").append('<option value="'+dataModel[i].ProductModelID+'">'+dataModel[i].Name+'</option>');
            }
            $('#productModel').select2();
        }        
        
        /**
         *  get all product by category id
         * @param {type} idCategory
         * @returns {undefined}
         */
        function getProductsByCategory(idCategory){
            $(".content-loader").css("display","block");
            $.ajax({
                method: "GET",
                url:'http://technicaltestj.azurewebsites.net/api/Product',
                data:{idCategory:idCategory},
                datatype: 'json'
            }).done(function(response){
                dataProductsArray = response;
                renderProducts(response);
                $(".content-loader").hide();
            }).fail(function(response) {
                $(".content-loader").hide();
                alert("An Error Occurred, please check the console for more information");
                console.log(response);
            });
        }
        
        /**
         * Get all products
         * @returns {undefined}
         */
        function getAllProducts()
        {
            $.ajax({
                method: "GET",
                url:'http://technicaltestj.azurewebsites.net/api/Product',
                datatype: 'json'
            }).done(function(response){
                renderProducts(response);
                $(".content-loader").hide();
            }).fail(function(response) {
                $(".content-loader").hide();
                alert("An Error Occurred, please check the console for more information");
                console.log(response);
            });
            
        }
        
        
        /**
         * render product to content of site
         * @param {type} dataProduct
         * @returns {undefined}
         */
        function renderProducts(dataProduct){
            
            $(".placeholders").html('');
            for(var i=0;i<dataProduct.length;i++){
                
                var dataImage = dataProduct[i].ThumbNailPhoto!=null?dataProduct[i].ThumbNailPhoto:'R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==';              
                
                $(".placeholders").append('<div class="col-xs-6 col-sm-3 placeholder">'
                    +'<a onclick="getProductDetails('+dataProduct[i].ProductID+')"><img src="data:image/jpg;base64,'+dataImage+'" width="200px" height="200px" class="img-responsive thumbnailImage" alt="'+dataProduct[i].ThumbnailPhotoFileName+'"></a>'
                    +'<h4>'+dataProduct[i].Name+'</h4>'
                    +'</div>');
            }
        }
        
        /**
         * Get al information about a product by id 
         * @param {type} id
         * @returns {undefined}
         */
        function getProductDetails(id){
            $(".content-loader").css("display","block");
            $.ajax({
                method: "GET",
                url:'http://technicaltestj.azurewebsites.net/api/Product',
                data:{id:id},
                datatype: 'json'
            }).done(function(response){
                
                renderProductDetails(response);
                $(".content-loader").hide();
            }).fail(function(response) {
                $(".content-loader").hide();
                alert("An Error Occurred, please check the console for more information");
                console.log(response);
            });
        }
        
        
        /**
         * Render information about product
         * @param {type} dataProduct
         * @returns {undefined}
         */
        function renderProductDetails(dataProduct){
            var dataImage = dataProduct.ThumbNailPhoto!=null?dataProduct.ThumbNailPhoto:'R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==';              
            
            $('#myModal').find('.modal-body').html('<div class="left-modal-body"> '
                                                    +'<img src="data:image/jpg;base64,'+dataImage+'" width="500" height="500" class="img-responsive" alt=""></a>'
                                                    +'</div>'
                                                    +'<div class="right-modal-body">'
                                                    +'<h2>'+dataProduct.Name+'</h2>'
                                                    +'<h3>$'+dataProduct.StandardCost.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") +'</h3>'
                                                    +'<h4>Size:'+dataProduct.Size+'</h4>'
                                                    +'<h4>Category:'+dataProduct.ProductCategory.Name+'</h4>'
                                                    +'<h4>Catalog:'+dataProduct.ProductModel.Name+'</h4>'
                                                    +'<h4>Description:'+dataProduct.ProductModel.Description+'</h4>'
                                                    +'</div>');
            $('#myModal').modal('toggle');
            $('#myModal').modal('show');
        }
        
        
        /**
         * Show form add new product in modal
         * @returns {undefined}
         */
        function addNewProduct(){
            
            $('#productModal').modal('toggle');
            $('#productModal').modal('show');
        }
        
        /**
         *  validated and send a request for save the new product
         * @returns {undefined}
         */
        function saveProduct(){
             $( "#addProductForm" ).validate({
                rules: {
                  productName: {
                    required: true
                  },
                  productNumber: {
                    required: true
                  },
                  productCost: {
                    required: true
                  },
                  listPrice: {
                    required: true
                  },
                  sellStartDate: {
                    required: true
                  }
                }
            });
            
            
            if($("#addProductForm").valid()){
                $(".content-loader").css("display","block");
                var dataForm = {
                  productName :$('#productName').val(),
                  productNumber :$('#productNumber').val(),
                  productColor :$('#productColor').val(),
                  productCost :$('#productCost').val(),
                  listPrice :$('#listPrice').val(),
                  size:$('#size').val(),
                  weight:$('#weight').val(),
                  productCategory:$('#productCategory').val(),
                  productModel:$('#productModel').val(),
                  sellStartDate:$('#sellStartDate').val(),
                  sellEndDate:$('#sellEndDate').val(),
                  discontinuedDate:$('#discontinuedDate').val(),
                  thumbNailPhotoFileName:$('#thumbnailPhotoFileName').val(),  
                  thumbNailPhoto:dataArray,
                  
                };
                
                $.ajax({
                    method: "POST",
                    url: 'http://technicaltestj.azurewebsites.net/api/Product',
                    data: JSON.stringify(dataForm),
                    contentType: 'application/json; charset=utf-8',
                    datatype: 'json'
                }).done(function(response){
                    $('#productModal').modal('toggle');
                    location.reload();
                    
                }).fail(function(response) {
                    $(".content-loader").hide();
                    alert("An Error Occurred, please check the console for more information");
                    console.log(response);
                });
                
                
                
            }
            
        }
      